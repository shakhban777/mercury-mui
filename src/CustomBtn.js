import {useStyles} from "./themes/useStyles";
import {Button} from "@material-ui/core";

export const CustomBtn = (props) => {
  const classes = useStyles(props);
  const {error, ...properties} = props;

  if (error) {
    return <Button {...properties} variant="contained" className={classes.error}/>;
  } else {
    return <Button {...properties} />;
  }
}