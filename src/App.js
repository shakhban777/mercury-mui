import React from "react";
import {Grid, Paper, ThemeProvider,} from "@material-ui/core";
import {useMuiTheme} from "./themes/useMuiTheme";
import {CustomBtn} from "./CustomBtn";
import './App.css';

function App() {
  const theme = useMuiTheme();

  return (
    <ThemeProvider theme={theme}>
      <Grid container
            justify="center"
            alignItems="center">
        <Paper width={theme.spacing(10)}>
          <Grid container
                alignItems="center"
                spacing={4}>
            <Grid item sm={12} xs={6}>
              <CustomBtn>Simple Button</CustomBtn>
            </Grid>
            <Grid item sm={12} xs={6}>
              <CustomBtn variant="contained" color="primary">Primary Button</CustomBtn>
            </Grid>
            <Grid item sm={12} xs={6}>
              <CustomBtn variant="contained" color="secondary">Secondary Button</CustomBtn>
            </Grid>
            <Grid item sm={12} xs={6}>
              <CustomBtn error>Error Button</CustomBtn>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </ThemeProvider>
  );
}

export default App;
