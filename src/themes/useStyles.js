import {makeStyles} from "@material-ui/core";

export const useStyles = makeStyles(theme => ({
  error: {
    backgroundColor: theme.palette.error.main,
    "&:hover": {
      backgroundColor: theme.palette.error.dark,
    },
    color: theme.palette.common.white
  }
}));