import {createMuiTheme} from "@material-ui/core";

export const useMuiTheme = () => {
  return createMuiTheme({
    spacing: 4,

    breakpoints: {
      values: {
        xs: 0,
        sm: 700,
        md: 960,
        lg: 1280,
        xl: 1920,
      },
    },

    typography: {
      button: {
        fontSize: 16,
        fontWeight: 700
      }
    },

    props: {
      MuiButton: {
        fullWidth: true,
      }
    },

    palette: {
      primary: {
        main: "#0F4780"
      },
      secondary: {
        main: "#6B64FF"
      },
    },

    overrides: {
      MuiButton: {
        root: {
          padding: "10px 16px",
        },
        containedSecondary: {
          "&:hover": {
            opacity: 0
          }
        }
      },

      MuiPaper: {
        root: {
          width: 400,
          padding: 20,
          margin: 20
        },
        rounded: {
          borderRadius: "8px"
        }
      }
    },
  });
}